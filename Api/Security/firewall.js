import UserQueries from "../Users/query";
import expressJwt from 'express-jwt'
import config from '../config/server'

const firewall = (roles = []) => {
	// roles param can be a single role string (e.g. Role.User or 'User') 
    // or an array of roles (e.g. [Role.Admin, Role.User] or ['Admin', 'User'])
	if (typeof roles === 'string') {
        roles = [roles];
    }

    return [
        // authenticate JWT token and attach user to request object (req.user)
        expressJwt({ secret: config.secret }),

        // authorize based on user role
        async (req, res, next) => {
        	let token = req.headers.authorization.split(' ')[1]
        	let userValidToken = await UserQueries.getValidTokenByUserId(req.user.id)
            if (token !== userValidToken || (roles.length && !roles.includes(req.user.role))) {
                // user's role is not authorized
                return res.status(401).json({ message: 'Unauthorized' });
            }

            // authentication and authorization successful
            next();
        }
    ];
}

export default firewall