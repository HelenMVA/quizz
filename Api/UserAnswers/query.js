import { database } from "../setup/database";

const UserAnswerQueries = {
	createUserAnswer: (step, gameId, questionId) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql = `INSERT INTO \`user_answers\` (step, game_id, question_id) VALUES (${step}, ${gameId}, ${questionId})`
			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve()
			})
		})
	},
	setPoints: (user, valid) => {
		let connection = database.connect()
		let points = 
			(valid && user.step === 0) ?
				4
				:((valid && user.step === 1)?
					2
					:((valid && user.step === 2)?
						1
						:0)
				)

		return new Promise((resolve, reject) => {
			let sql = `UPDATE \`user_answers\` AS ua SET points = ${points} WHERE ua.game_id = ${user.gameId} AND ua.step = ${user.question}`
			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve(points)
			})
		})
	}
}

export default UserAnswerQueries