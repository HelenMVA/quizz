import express from "express"
import usersRoutes from "../Users/router"
import gamesRoutes from "../Games/router"

const Router = (server) => {
    //Project router
    server.use('/api/user', usersRoutes)
    server.use('/api/game', gamesRoutes)

    // Home route. We'll end up changing this to our main front end index later.
    // server.use('/', function (req, res) {
    //     return res.status(200).sendFile('index.html', {root: './src'})
    // });

    // server.use('/api/project', projectRoutes);
    // server.use('/api/pages', pagesRoutes);
    
}

export default Router