INSERT INTO `questions` (title) VALUES
('De quel standard est adapté le Javascript ?'),

('Quelles types d\'erreurs existent en Javascript ?'),

('Où peut‐on écrire du Javascript ?'),

('Quelle syntaxe est correcte ?'),

('Quelle syntaxe est correcte ?'),

('Quelle syntaxe est correcte ?'),

('Quelle syntaxe est correcte ?'),

('Comment accéder au 3eme élément du tableau eleves[] ?'),

('Un objet (Object) se déclare avec les caractères :'),

('Pour accéder à la propiété \'couleur\' de l\'objet Frigo, on peut utiliser :'),

('Quelle méthode n\'existe pas ?'),

('Quelle méthode permet d\'exécuter la fonction go() quand le click sur le document est détécter ?'),

('Quel événement doit être utilisé pour vérifier la saisi d\'un input en temps réel ?'),

('Qu\'affiche le code ci‐dessous ? (chaque retour de console log est représenté par //)
	
var items = [1, 2, 3]
var copie = []
items.forEach(function(item){
  copie.push(item)
})
for (var i = 0\; i < copie.length\; i++) {
  console.log(copie[i])
}'),

('Comment changer le taille du texte du premier paragraphe ?'),

('Qu\'affiche le code ci‐dessous ? (chaque retour de console log est représenté par //)

var i = 7
function change() {
    var i = 5
    console.log(i)
}
console.log(i)
change()');

INSERT INTO `answers` (title, is_valid, question_id) VALUES
('ECMA Script', 1, 1),
('Live Script', 0, 1),
('Java', 0, 1),
('Standard Script', 0, 1),

('Reference Error', 1, 2),
('Stack Error', 0, 2),
('Stack Overflow Error', 0, 2),
('External Error', 0, 2),

('Entre les balises <script>....</script>', 1, 3),
('Entre les balises <head>....</head>', 0, 3),
('Entre les balises <p>....</p>', 0, 3),
('Dans le css', 0, 3),

('if (a ! = 2) {}', 1, 4),
('if a ! = 2 {}', 0, 4),
('if (a ! = 2) []', 0, 4),
('if (a ≠ 2) {}', 0, 4),

('for i=0\; i<10\; i++ {}', 0, 5),
('for (let i=0\; i<10\; i++) {}', 1, 5),
('for (int i=0\; i<10) {}', 0, 5),
('for (let i=0, i<10, i++) {}', 0, 5),


('var age = ((100/4)*2)+\'ans\'', 1, 6),
('var age = 50 ans', 0, 6),
('var age = ((100/4)*2).\'balais\'', 0, 6),
('var age = 5O.\'ans\'', 0, 6),

('var nom = "Timmy"', 1, 7),
('var \'nom\' = Timmy', 0, 7),
('var nom = \'Timmy"', 0, 7),
('var "nom" = "Timmy"', 0, 7),

('eleves[2]', 1, 8),
('eleves->3', 0, 8),
('eleves[3]', 0, 8),
('eleves{2}', 0, 8),

('{}', 1, 9),
('[]', 0, 9),
('""', 0, 9),
('()', 0, 9),

('Frigo["couleur"]', 1, 10),
('Frigo->couleur', 0, 10),
('Frigo->getCouleur()', 0, 10),
('(Frigo->get("couleur"))', 0, 10),

('document.getElementsByAttribut', 1, 11),
('document.getElementsByTagName', 0, 11),
('document.getElementsByClassName', 0, 11),
('document.getElementById', 0, 11),

('document.addEventListener("click", go)', 1, 12),
('document.addEvent("click", go)', 0, 12),
('document.click(go)', 0, 12),
('document.addListenEvent("click", go)', 0, 12),

('keyup', 1, 13),
('keydown', 0, 13),
('change', 0, 13),
('check', 0, 13),

('// 1 // 2 // 3', 1, 14),
('// 3 // 2 // 1', 0, 14),
('// 1 // 2 // 3 // 1 // 2 // 3', 0, 14),
('// 1 // 2 // 3 // 3 // 2 // 1', 0, 14),

('document.getElementsByTagName("p")[0].style.fontSize="30px"', 1, 15),
('document.getElementsByTagName("p").style.font‐size="30px"', 0, 15),
('document.getElementsByTagName("p")[0].style.font‐size=30', 0, 15),
('document.getElementByTagName(p)[1].setStyle("font‐size")=30px', 0, 15),

('// 7 // 5', 1, 16),
('// 5 // 7 // 5', 0, 16),
('// 5 // 7 // 7', 0, 16),
('// 7 // 7', 0, 16);