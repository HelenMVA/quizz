CREATE TABLE `users` ( 
	`id` INT NOT NULL AUTO_INCREMENT, 
	`username` VARCHAR(100) NOT NULL, 
	`email` VARCHAR(75) NOT NULL, 
	`password` VARCHAR(255) NOT NULL,
	`role` VARCHAR(20) NOT NULL,
	`valid_token` VARCHAR(255),
	 PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `questions` ( 
	`id` INT NOT NULL AUTO_INCREMENT, 
	`title` TEXT NOT NULL, 
	 PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `answers` ( 
	`id` INT NOT NULL AUTO_INCREMENT, 
	`title` TEXT NOT NULL, 
	`is_valid` BOOLEAN  NOT NULL, 
	`question_id` INT NOT NULL,
	 PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `games` ( 
	`id` INT NOT NULL AUTO_INCREMENT, 
	`score` INT NOT NULL, 
	`duration` TIME, 
	`ratio` DECIMAL(2,2),
	`user_id`INT NOT NULL,
	 PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `user_answers` ( 
	`id` INT NOT NULL AUTO_INCREMENT,
	`step` INT NOT NULL,
	`answer` TEXT, 
	`duration` TIME,
	`points` INT,
	`game_id` INT NOT NULL, 
	`question_id` INT NOT NULL , 
	`answer_id` INT,
	 PRIMARY KEY (`id`)
) ENGINE = InnoDB;

ALTER TABLE `answers`
	ADD CONSTRAINT `answer_question_ibfk1` 
	FOREIGN KEY (`question_id`) 
	REFERENCES `questions`(`id`);

ALTER TABLE `games`
	ADD CONSTRAINT `game_user_ibfk1` 
	FOREIGN KEY (`user_id`) 
	REFERENCES `users`(`id`);

ALTER TABLE `user_answers`
	ADD CONSTRAINT `userAnswer_game_ibfk1` 
	FOREIGN KEY (`game_id`) 
	REFERENCES `games`(`id`);

ALTER TABLE `user_answers`
	ADD CONSTRAINT `userAnswer_question_ibfk1` 
	FOREIGN KEY (`question_id`) 
	REFERENCES `questions`(`id`);

ALTER TABLE `user_answers`
	ADD CONSTRAINT `userAnswer_answer_ibfk1` 
	FOREIGN KEY (`answer_id`) 
	REFERENCES `answers`(`id`);