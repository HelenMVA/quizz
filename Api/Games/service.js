import GameQuery from './query'
import QuestionQueries from '../Questions/query'
import UserAnswerQueries from '../UserAnswers/query'
import UserQueries from '../Users/query'
import jwt from 'jsonwebtoken'
import config from "../config/server.json"

const GameService = {
	initialisation: (user) => {
		return new Promise((resolve, reject) => {
			GameQuery.createNewGame(user.id)
			.then(async gameId => {
				let questions = await QuestionQueries.getRandomQuestions(10)
				let token = signGameToken(user, gameId, questions.length)
				for (let i = 0, l = questions.length; i < l; i++) 
					await UserAnswerQueries.createUserAnswer(i, gameId, questions[i].id)
				
				await UserQueries.registerUserToken(user.id, token)
				return resolve(token)
			})
			.catch(err => reject(err))
		})
	},
	getQuestion: (user) => {//S'occupe également de terminer la partie, pourrait être renommer en 'runGame'
		return new Promise((resolve, reject) => {
			if (user.question < user.limit) {
				GameQuery.getQuestion(user.gameId, user.question)
				.then(async question => resolve(question))
				.catch(err => reject(err))
			}
			else {
				GameQuery.endGame(user.gameId)
				.then(async results => {
					let ratio = results.score/(user.limit*4)
					let token = jwt.sign({ id: user.id, role: user.role }, config.secret)
					if (ratio >= 2/3) {
						results.gif = "https://i.gifer.com/Rgq.gif"
						results.message = "Good!"
					}
					else if (ratio >= 1/3){
					 	results.gif = "https://i.gifer.com/5c2.gif"
					 	results.message = "Average!"
					}
					else {
						results.gif = "https://media3.giphy.com/media/26xBKqeFFspRZjDTW/giphy.gif"
						results.message = "Bad!"
					}
					await UserQueries.registerUserToken(user.id, token)
					return resolve({ results, token })
				})
				.catch(err => reject(err))
			}
		})
	},
	changeAnswers: (user) => {
		return new Promise((resolve, reject) => {
			switch (user.step) {
				case 0:
					return GameQuery.getAllAnswers(user.gameId, user.question)
					.then(async answers => {
						let token = signGameToken(user, user.gameId, user.limit, user.question, 1)
						await UserQueries.registerUserToken(user.id, token)
						return resolve({ token, answers })
					})
					.catch(err => reject(err))

				case 1:
					return GameQuery.getOneGoodAnswerAndOneBadRandomAnswer(user.gameId, user.question)
					.then(async answers => {
						let token = signGameToken(user, user.gameId, user.limit, user.question, 2)
						await UserQueries.registerUserToken(user.id, token)
						return resolve({ token, answers })
					})
					.catch(err => reject(err))

				default: return resolve(false)
			}	
		})
	},
	answerAQuestion: (user, answer) => {
		return new Promise((resolve, reject) => {
			GameQuery.tcheckUserAnswer(user, answer)
			.then(async result => {
				let token = signGameToken(user, user.gameId, user.limit, (user.question + 1), 0)
				let points = await UserAnswerQueries.setPoints(user, result.valid)
				if (result.valid) result.valid = points
				await UserQueries.registerUserToken(user.id, token)
				return resolve({ result, token })
			})
			.catch(err => reject(err))
		})
	}
}

function signGameToken(user, gameId, limit = 10, question = 0, step = 0) {
	return jwt.sign(
        { 
        	id: user.id, 
        	role: user.role, 
        	gameId,
        	limit,
        	question,
        	step
        },
        config.secret
     )
}

export default GameService