import { database } from "../setup/database";

const GameQueries = {
	createNewGame: (userId) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql = `INSERT INTO \`games\` (score, user_id) VALUES (0, ${userId});`

			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve(data.insertId)
			})
		})
	},
	getQuestion: (gameId, questionStep) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql = `SELECT q.title FROM \`games\` AS g LEFT JOIN \`user_answers\` `
				+ `AS ua ON ua.game_id = g.id LEFT JOIN \`questions\` AS q ON ua.question_id = q.id `
				+ `WHERE g.id = ${gameId} AND ua.step = ${questionStep};`

			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve(data[0])
			})
		})
	},
	getAllAnswers: (gameId, questionStep) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql = `SELECT a.title, a.id FROM \`games\` AS g LEFT JOIN \`user_answers\` `
				+ `AS ua ON ua.game_id = g.id LEFT JOIN \`answers\` AS a ON ua.question_id = a.question_id `
				+ `WHERE g.id = ${gameId} AND ua.step = ${questionStep} ORDER BY RAND();`

			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve(data)
			})
		})
	},
	getOneGoodAnswerAndOneBadRandomAnswer: (gameId, questionStep) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql1 = `SELECT a.title, a.id FROM \`games\` AS g LEFT JOIN \`user_answers\` `
				+ `AS ua ON ua.game_id = g.id LEFT JOIN \`answers\` AS a ON ua.question_id = a.question_id `
				+ `WHERE g.id = ${gameId} AND ua.step = ${questionStep} AND a.is_valid = 1 LIMIT 1;`

			let sql2 = `SELECT a.title, a.id FROM \`games\` AS g LEFT JOIN \`user_answers\` `
				+ `AS ua ON ua.game_id = g.id LEFT JOIN \`answers\` AS a ON ua.question_id = a.question_id `
				+ `WHERE g.id = ${gameId} AND ua.step = ${questionStep} AND a.is_valid = 0 ORDER BY RAND() LIMIT 1;`

			connection.query(sql1, (err, result1) => {
				if (err) {
					connection.end()
					return reject(err)
				}
				else {
					connection.query(sql2, (err, result2) => {
						connection.end()
						if (err) return reject(err)
						else return resolve([result1[0], result2[0]])
					})
				}
			})
		})
	},
	tcheckUserAnswer: (user, answer) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let where,
				getSql = (where = '') => {
					return `SELECT a.title, a.id FROM \`games\` AS g LEFT JOIN \`user_answers\` `
						+ `AS ua ON ua.game_id = g.id LEFT JOIN \`answers\` AS a ON ua.question_id = a.question_id `
						+ `WHERE g.id = ${user.gameId} AND ua.step = ${user.question} AND is_valid = 1${where};`
				}

			switch (typeof answer) {
				case 'string':
					where = ` AND a.title LIKE '${answer}'`
					break;

				case 'number':
					where = ` AND a.id = ${answer}`
					break;

				default:
					connection.end()
					return resolve(false)
			}

			connection.query(getSql(where), (err, data) => {
				if (err) {
					connection.end()
					return reject(err)
				}
				else {
					if (!data[0]) {
						connection.query(getSql(), (err, data) => {
							connection.end()
							if (err) return reject(err)
							else return resolve({ valid: false, correction: data[0] })
						})
					}
					else {
						connection.end()
						return resolve({ valid: data[0] })
					}
				}
			})
		})
	},
	endGame: (gameId) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let scoreSql = `SELECT SUM(ua.points) AS score FROM \`games\` AS g LEFT JOIN \`user_answers\` `
				+ `AS ua ON ua.game_id = g.id WHERE g.id = ${gameId}`
				
			connection.query(scoreSql, (err, data) => {
				if (err) {
					connection.end()
					return reject(err)
				}
				else {
					let sql = `UPDATE \`games\` SET score = ${data[0].score} WHERE id = ${gameId}`
					connection.query(sql, (err) => {
						connection.end()
						if (err) return reject(err)
						else return resolve(data[0])
					})
				}
			})
		})
	}
}

export default GameQueries
