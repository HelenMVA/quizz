import express from 'express'
import GameController from './controller'
import firewall from '../Security/firewall'

const router = express.Router()

router.post('/', firewall('USER'), GameController.initialisation)
router.post('/getQuestion', firewall('USER'), GameController.getQuestion)
router.post('/changeAnswers', firewall('USER'), GameController.changeAnswers)
router.post('/answerAQuestion', firewall('USER'), GameController.answerAQuestion)

export default router