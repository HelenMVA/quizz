import GameService from './service'

const GameController = {
	initialisation: (req, res) => {
		GameService.initialisation(req.user)
		.then(gameToken => res.status(200).send(gameToken))
		.catch(err => res.status(400).send(err))
	},
	getQuestion: (req, res) => {
		GameService.getQuestion(req.user)
		.then(gameData => res.status(200).send(gameData))
		.catch(err => res.status(400).send(err))
	},
	changeAnswers: (req, res) => {
		GameService.changeAnswers(req.user)
		.then(gameData => res.status(200).send(gameData))
		.catch(err => res.status(400).send(err))
	},
	answerAQuestion: (req, res) => {
		let answerKeyWord = req.body.answerKeyWord
		let answerId = req.body.answerId ? parseInt(req.body.answerId, 10) : null
		GameService.answerAQuestion(req.user, (answerId)? answerId : answerKeyWord)
		.then(gameData => res.status(200).send(gameData))
		.catch(err => res.status(400).send(err))
	}
}

export default GameController