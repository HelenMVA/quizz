import { database } from "../setup/database";

const QuestionQueries = {
	getRandomQuestions: (limit) => {
		let connection = database.connect()

		return new Promise((resolve, reject) => {
			let sql = `SELECT * FROM \`questions\` ORDER BY RAND() LIMIT ${limit};`
			
			connection.query(sql, (err, data) => {
				connection.end()
				if (err) return reject(err)
				else return resolve(data)
			})
		})
	}
}

export default QuestionQueries