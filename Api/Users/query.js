import db, { database } from "../setup/database";

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
  authenticate: (user, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM users WHERE name="${user.email}" AND password="${user.password}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows[0]);
      } else {
        return successCallback("Incorrect username or password combinaison");
      }
    });
  },
  register: async (users) => {
    return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO users (id, username, email, password, role) VALUES (NULL,"${users.username}", "${users.email}", "${users.hashedPassword}", "USER")`;

      db.query(sqlQuery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  },
  registerUserToken: (userId, token) => {
    let connection = database.connect()

    return new Promise((resolve, reject) => {
      let sql = `UPDATE \`users\` SET valid_token = '${token}' WHERE id = ${userId}`
    
      connection.query(sql, (err, data) => {
        connection.end()
        if (err) return reject(err)
        else return resolve(data)
      })
    })
  },
  getByUserEmail: (email) => {
    let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  getByUsername: (email) => {
    let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  getValidTokenByUserId: (userId) => {
    let sqlQuery = `SELECT valid_token FROM users WHERE id="${userId}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]['valid_token']);
      });
    });
  },
};

export default Queries;
