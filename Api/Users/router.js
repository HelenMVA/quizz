import express from "express";
import UserController from "./controller"
// import authorisation from "../../helpers/authorisation"

const router = express.Router();

// router.post('/register', (req, res) => {
// 	res.statut(200).send('coucou')
// })

router.post("/register", UserController.register);
router.post("/authenticate", UserController.authenticate);

export default router;
