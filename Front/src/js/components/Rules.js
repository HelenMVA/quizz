import React from "react";
import styled from "styled-components";
import Auth from "../store/Auth";
import { useSelector, useDispatch } from "react-redux";

const Rules = () => {
  const modal = useSelector((state) => state.authReducer.isModalShowing);

  const dispatch = useDispatch();
  const closeModalRules = () =>
    dispatch({
      type: "TOGGLE_IS_MODAL_SHOWING",
      payload: { type: null, title: null },
    });

  return (
    <ModalContainer>
      <div className="modal d-block overlay" tabIndex="-1" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Rules</h5>
              <button
                type="button"
                className="close"
                onClick={closeModalRules}
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body d-flex flex-wrap">
              <ul className="mt-4 ml-2 list-inline">
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Register and then login participate at a Quizz</span>
                  </i>
                </li>
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Click Play to access Quizz page</span>
                  </i>
                </li>
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Click Start to launch the Quizz</span>
                  </i>
                </li>
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Submit you're answer in the input and click send (gain 4 points)</span>
                  </i>
                </li>
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Display 4 choice by cliking on Make my life easier (gain 3 points)</span>
                  </i>
                </li>
                <li className="mt-3">
                  <i className="fa fa-asterisk" aria-hidden="true">
                    <span className="ml-2">Display 2 choice with More easy (gain 2 points)</span>
                  </i>
                </li>
              </ul>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-warning button"
                onClick={closeModalRules}
                data-dismiss="modal"
              >
                Ok
              </button>
            </div>
          </div>
        </div>
      </div>
    </ModalContainer>
  );
};

const ModalContainer = styled.div`
   {
    @import url("https://fonts.googleapis.com/css2?family=Revalia&display=swap");
    font-size: 1em;
    font-family: "Revalia", "Times New Roman", Times, serif;

    .overlay {
      background-color: rgba(255, 255, 255, 0.3);
    }
    .close {
      color: red;
    }

    .modal-content {
      min-height: 80vh;
      background-color: #003140 !important;
      color: #fff;
      border: 1px solid #f2f55b !important;
      border-radius: 10px;
    }

    .fa-asterisk {
      color: #f2f55b !important;

      span {
        color: #fff;
      }
    }
    .button {
      position:absolute;
      bottom:4em;
      right:4em;
      width: 13em;
      height: 5em;
      font-family: "Revalia", "Roboto", sans-serif;
      font-size: 0.6em;
      margin-top: 1em;
      text-transform: uppercase;
      letter-spacing: 2px;
      font-weight: 700;
      color: #fff;
      background-color: rgba(212, 214, 117, 0.1);
      border: 2px solid rgba(212, 214, 117, 0.8);
      border-radius: 35px;
      box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
      transition: all 0.3s ease 0s;
      cursor: pointer;
      outline: none;
    }
    .button:hover {
      background-color: #b8f46e;
      border: 1px solid gray;
      box-shadow: 0px 15px 20px rgba(183, 244, 110, 0.4);
      color: #fff;
      transform: translateY(-5px);
      text-shadow: 2px 2px gray;
    }
  }
`;

export default Rules;
