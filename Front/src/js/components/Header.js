import React from "react";
import styled from "styled-components";
import { BrowserRouter as Router, Link } from "react-router-dom";
const HeaderComponent = () => {
  return (
    <Nav>
      <div>
        <Link to="/">
          <img
            src={require("../../assets/img/simplon.png")}
            alt="Simplon"
            className="pl-3"
          />
        </Link>
      </div>
    </Nav>
  );
};

const Nav = styled.div`
  div {
    width: 100vw;
    height: 8vh;
    background-color: #003140;
    a {
      height: 8vh;
      display: flex !important;
      align-items: center !important;
      img {
        width: auto;
        height: 80%;
      }
    }
  }
`;

export default HeaderComponent;
