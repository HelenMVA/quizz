import React from "react";

// 3 modals
import Rules from "../components/Rules";
import Register from "../components/Register";
import Login from "../components/Login";

import { useSelector } from "react-redux";

const Modale = (props) => {
	const modal = useSelector((state) => state.authReducer.isModalShowing);

	return (
		<div>
			{modal.type === "rules" && <Rules />}
			{modal.type === "login" && <Login />}
			{modal.type === "register" && <Register />}
		</div>
	);
};

export default Modale;
