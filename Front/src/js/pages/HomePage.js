import React from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Link } from "react-router-dom";

// Modals
import Modale from "../pages/Modale";

const HomePage = () => {
  const dispatch = useDispatch();
  const showModalRules = () =>
    dispatch({
      type: "TOGGLE_IS_MODAL_SHOWING",
      payload: { type: "rules", title: "Rules" },
    });
  const showModalLogin = () =>
    dispatch({
      type: "TOGGLE_IS_MODAL_SHOWING",
      payload: { type: "login", title: "Login" },
    });
  const showModalRegister = () =>
    dispatch({
      type: "TOGGLE_IS_MODAL_SHOWING",
      payload: { type: "register", title: "Register" },
    });

  const user = useSelector((state) => state.authReducer.user);
  console.log(user);
  const initGame = () => {};

  return (
    <Body>
      {user ? <h1>{user.username} GO Quizz</h1> : <h1> Simplonizz GO Quizz</h1>}
      <div className="container-flex">
        <div className="row">
          <div className="col-lg-5 col-md-5 col-sm-12 mx-auto d-flex align-items-center flex-column justify-content-center">
            {!user ? (
              <div className="container-auth mb-5">
                <button className="button" onClick={showModalRegister}>
                  Sign Up
                </button>
                <button className="button ml-5" onClick={showModalLogin}>
                  Sign In
                </button>
              </div>
            ) : (
              <span>
                <button className="button  mb-5" onClick={showModalRules}>
                  Rules
                </button>
                <br />
                <Link to="/game">
                  <button className="button">PLAY</button>
                </Link>
              </span>
            )}
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12 gif">
            <img src={require("../../assets/img/avento.gif")} alt="Simplon" />
          </div>
        </div>
      </div>
    </Body>
  );
};

const Body = styled.div`
   {
    @import url("https://fonts.googleapis.com/css2?family=Revalia&display=swap");
    width: 100vw;
    height: 92vh;
    background-color: #003140;
    .row {
      margin: 0 !important;
      .gif {
        display: flex !important;
        align-items: center !important;
        justify-content: flex-start;
        img {
          height: 70%;
          width: auto;
          max-width: 100%;
        }
      }
    }
    h1 {
      color: white;
      text-align: center;
      font-family: "Revalia", "Times New Roman", Times, serif;
      letter-spacing: 0.05em;
      text-shadow: 2px 2px 4px rgba(255, 255, 255, 0.5);
    }
    .button {
      width: 15em;
      height: 5em;
      font-family: "Revalia", "Roboto", sans-serif;
      font-size: 0.8em;
      margin-top: 2em;
      text-transform: uppercase;
      letter-spacing: 2.5px;
      font-weight: 700;
      color: #fff;
      background-color: rgba(212, 214, 117, 0.1);
      border: 2px solid rgba(212, 214, 117, 0.8);
      border-radius: 35px;
      box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
      transition: all 0.3s ease 0s;
      cursor: pointer;
      outline: none;
    }
    .button:hover {
      background-color: #b8f46e;
      border: 1px solid gray;
      box-shadow: 0px 15px 20px rgba(183, 244, 110, 0.4);
      color: #fff;
      transform: translateY(-5px);
      text-shadow: 2px 2px gray;
    }
  }
`;

export default HomePage;
