import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import instance, { addAuth } from "../../utils/api";

const QuizzPage = () => {
  const user = useSelector((state) => state.authReducer.user);

  const [title, setTitle] = useState(null);
  const [choice, setChoice] = useState(null);
  const [answer, setAnswer] = useState(null);
  const [start, setStart] = useState(true);
  const [valid, setValid] = useState(null);
  const [correction, setCorrection] = useState(null);
  const [id, setId] = useState(null);
  const [point, setPoint] = useState(null);
  const [easy, setEasy] = useState(true);
  const [send, setSend] = useState(true);
  const [totalPoint, setTotalPoint] = useState(null);
  const [messageScore, setMessageScore] = useState(null);
  const [gif, setGif] = useState(null);
  const [makeChoice, setMakeChoice] = useState(null);
  const [next, setNext] = useState(false);

  const initGame = () => {
    instance.post("game").then((res) => {
      addAuth(res.data);
    });
  };

  const getQuestion = () => {
    instance.post("game/getQuestion").then((res) => {
      setTitle(res.data.title);
      setChoice(null);
      setCorrection(null);
      setValid(null);
      setPoint(null);
      setEasy(true);
      setSend(true);
      setStart(false);
      setNext(false);
      if (!res.data.title) {
        setTotalPoint(res.data.results.score);
        setMessageScore(res.data.results.message);
        setGif(res.data.results.gif);
      }
      let input = document.getElementById("toggle-input");
      input.classList.remove("show-input");
    });
  };
  const changeChoice = () => {
    instance.post("game/changeAnswers").then((res) => {
      addAuth(res.data.token);
      setChoice(res.data.answers);
    });
    let input = document.getElementById("toggle-input");
    input.classList.add("show-input");
  };
  const changeChoice2 = () => {
    instance.post("game/changeAnswers").then((res) => {
      addAuth(res.data.token);
      setChoice(res.data.answers);
      setEasy(false);
    });
    let input = document.getElementById("toggle-input");
    input.classList.add("show-input");
  };
  const checkAnswer = () => {
    if (!id && !answer) {
      setMakeChoice("Please choose an answer");
      setTimeout(() => setMakeChoice(null), 3000);

      return;
    }
    instance
      .post("game/answerAQuestion", {
        answerId: id ? id : null,
        answerKeyWord: answer ? answer : null,
      })
      .then((res) => {
        addAuth(res.data.token);
        if (res.data.result.valid === false) {
          setCorrection(res.data.result.correction.title);
        }
        setValid(res.data.result.valid);
        setPoint(res.data.result.valid);
        setEasy(false);
        setSend(false);
        setNext(true);
        document.getElementById("clear-input").value = "";
        let input = document.getElementById("toggle-input");
        input.classList.add("show-input");
      });
  };
  useEffect(() => {
    initGame();
  }, []);

  return (
    <Body>
      {start && (
        <span>
          {user ? (
            <h2>May the force be with you {user.username}</h2>
          ) : (
            <h6> Good Luck</h6>
          )}
          <button className="button mb-4" onClick={getQuestion}>
            START
          </button>
        </span>
      )}
      {title && (
        <span>
          <h3>{title}</h3>
          <br></br>
          <div id="toggle-input">
            <input
              id="clear-input"
              type="text"
              className="form-control w-25 mx-auto mt-4 mb-2"
              placeholder="answer"
              aria-describedby="basic-addon2"
              onChange={(e) => setAnswer(e.target.value)}
            ></input>
            {send && (
              <button className="mb-2 button" onClick={checkAnswer}>
                Send
              </button>
            )}
            <br></br>
            {easy && (
              <button onClick={changeChoice} className="mb-2 button">
                Make my life easier please
              </button>
            )}
          </div>
          {choice &&
            choice.map((answer) => (
              <div key={answer.id} id="choiceContainer">
                <label class="mt-3">
                  <input
                    class="mr-2"
                    type="radio"
                    value={answer.id}
                    name="answerId"
                    onClick={(e) => setId(e.target.value)}
                  />
                  {answer.id}  {answer.title}
                </label>
              </div>
            ))}
          {choice && (
            <span>
              {send && (
                <button onClick={checkAnswer} className="mb-2 button">
                  Send
                </button>
              )}
              {easy && (
                <button onClick={changeChoice2} className="mb-2 button">
                  More easier
                </button>
              )}
            </span>
          )}
          {makeChoice && <p>{makeChoice}</p>}
          {valid === false && <h6 className="mb-2 wrong">Wrong answer</h6>}
          {correction && (
            <h6 className="mb-2 correction"> Correction :{correction}</h6>
          )}
          {point && <h6 className="mb-2 good"> good answer + {point}points</h6>}
          {next && (
            <button onClick={getQuestion} className="mb-2 button">
              Next
            </button>
          )}
        </span>
      )}
      {totalPoint && messageScore && gif && (
        <span class="mt-5">
          <h6 cass="mt-5"> Your total Score is {totalPoint}</h6>
          <p cass="mt-5"> {messageScore}</p>
          <img cass="mt-5" src={gif} alt="Simplon" className="pl-3" />
        </span>
      )}
    </Body>
  );
};

const Body = styled.div`
   {
    @import url("https://fonts.googleapis.com/css2?family=Revalia&display=swap");
    width: 100vw;
    height: 92vh;
    background-color: #003140;
    color: white;
    text-align: center;
    font-family: "Revalia", "Times New Roman", Times, serif;
    .button {
      height: 4em;
      font-family: "Revalia", "Roboto", sans-serif;
      font-size: 0.8em;
      margin-top: 2em;
      padding: 5px 30px;
      text-transform: uppercase;
      letter-spacing: 2.5px;
      font-weight: 700;
      color: #fff;
      background-color: rgba(212, 214, 117, 0.1);
      border: 2px solid rgba(212, 214, 117, 0.8);
      border-radius: 35px;
      box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
      transition: all 0.3s ease 0s;
      cursor: pointer;
      outline: none;
    }

    .button:hover {
      background-color: #b8f46e;
      border: 1px solid gray;
      box-shadow: 0px 15px 20px rgba(183, 244, 110, 0.4);
      color: #fff;
      transform: translateY(-5px);
      text-shadow: 2px 2px gray;
    }
    .show-input {
      display: none;
    }
    .wrong {
      color: red !important;
    }
    .correction {
      color: green !important;
    }
    .good {
      color: green !important;
    }
  }
`;

export default QuizzPage;
