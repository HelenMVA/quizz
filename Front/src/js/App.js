import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import HomePage from "./pages/HomePage";
import QuizzPage from "./pages/QuizzPage";
import Modale from "./pages/Modale";
import HeaderComponent from "./components/Header";

const App = () => {
  const dispatch = useDispatch();
  const token = useSelector((state) => state.authReducer.token);
  const isModalShowing = useSelector(
    (state) => state.authReducer.isModalShowing
  );

  const onClick = (e) => {
    dispatch({
      type: "SET_AUTH_TOKEN",
      payload: true,
    });
  };

  const secondClick = (e) => {
    dispatch({
      type: "CLEAR_AUTH_TOKEN",
    });
  };

  return (
    <Router>
      <HeaderComponent />

      {isModalShowing && <Modale />}

      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/game">
          <QuizzPage />
        </Route>
        <Router path="/Register">
          <h2>REGISTER</h2>
        </Router>
      </Switch>
    </Router>
  );
};

export default App;
